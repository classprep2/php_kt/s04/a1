<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>S4 Activity</title>
</head>
<body>

	<h1>Building</h1>
	<p><?php echo $building->printName(); ?>.</p>
	<p>The <?php echo $building->getName(); ?> has <?php echo $building->getFloor(); ?> floors.</p>
	<p>The <?php echo $building->getName(); ?> is located at <?php echo $building->getAddress(); ?>. </p>
	<?php $building->setName('Caswyn Complex'); ?>
	<p>The name of the building has been changed to  <?php echo $building->getName();?>.</p>

	<h1>Condominium</h1>
	<p><?php echo $condominium->printName(); ?>.</p>
	<p>The <?php echo $condominium->getName(); ?> has <?php echo $condominium->getFloor(); ?> floors.</p>
	<p>The <?php echo $condominium->getName(); ?> is located at <?php echo $condominium->getAddress(); ?>. </p>
	<?php $condominium->setName('Enzo Tower'); ?>
	<p>The name of the condominium has been changed to  <?php echo $condominium->getName();?>.</p>





</body>
</html>