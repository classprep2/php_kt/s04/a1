<?php


// Object from Classes
class Building {
	/* attributes and properties of a building*/
	protected $name;
	public $floors;
	public $address;

	// Constructor
	public function __construct($name, $floors, $address){
		$this->name=$name;
		$this->floors = $floors;
		$this->address=$address;
	}

	public function printName(){
		return "The name of the building is $this->name";
	}

	public function getName(){
		return $this->name;
	}

	public function getFloor(){
		return $this->floors;
	}

	public function getAddress(){
		return $this->address;
	}

	public function setName($name){
		$this->name=$name;
	}

}

// Instance / Instantiate
$building = new Building('Caswyn Building', 8, 'Timog Avenue, Quezon City, Philippines');

/* Inheritance and Polymorphism */
/* Parent - gives property and method to child */
/* Child - inherits methods and child*/

/* CHILD */
class Condominum extends Building{
	public function printName(){
		return "The name of the Condominum is $this->name";
	}

	/* ENCAPSULATION */
	// getters(read-only) && setters(write-only)
	// Encapsulating properties
	/* Also helps us to make data to be hidden*/
	public function getName(){
		return $this->name;
	}

	public function getFloor(){
		return $this->floors;
	}

	public function setName($name){
		$this->name=$name;
	}

	public function getAddress(){
		return $this->address;
	}
	
}

$condominium = new Condominum('Enzo Condo', 5, 'Buendia Avenue, Makati City Philippines');

?>
